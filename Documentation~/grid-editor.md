# Grid System - Documentation - Grid Editor

The Grid editor is meant to draw a grid layout, defining which nodes are crossable or not.

![Grid editor window](./Images/grid-editor.png)

## Layout assets

First, create a `Grid 2D Layout` asset from *Create > Grid System > Grid Layout 2D*.

### Options

- **Width**: The size of the grid, along the X axis.
- **Height**: The size of the grid, along the Y axis.
- **Walls**: The list of all the walls on the grid. You should edit it from the editor window instead of doing it manually. Just double-click on a `Grid 2D Layout` asset to open the editor.

## Editor

On the editor window, use the left-click of your mouse over the nodes you want to make crossable, and use the right-click to draw the non-crossable nodes.

## Scripting API

### `Grid2DLayoutAsset`

#### Methods

##### `AddWallAt()`

```cs
public void AddWallAt(int x, int y)
```

Adds a wall at the given position.

##### `IsWall()`

```cs
public bool IsWall(int x, int y)
```

Checks if the node at the given position is a wall (non-crossable).

##### `RemoveWallAt()`

```cs
public void RemoveWallAt(int x, int y)
```

Removes a wall at the given position.

#### Properties

##### `Width`

```cs
public int Width { get; }
```

The size of the grid along the X axis.

##### `Height`

```cs
public int Height { get; }
```

The size of the grid along the Y axis.

##### `Walls`

```cs
public Vector2Int[] Walls { get; }
```

Contains the position of all the non-crossable nodes on the grid.

---

[<= Back to homepage](../README.md)