# Grid System

Library for creating 2D grids and generate them in a scene. You can use this content to experiment with pathfinding techniques and algorithms.

## Features

- Grid editor

![Grid editor window](./Documentation~/Images/grid-editor.png)

## Install the package

Install this package from the *Package Manager* window (from `Window > Package Manager`), by clicking the *+* button, select *Add package from git URL*, and type the URL of this repository:

```txt
https://gitlab.com/formations2/e-artsup/gridsystem.git
```

![Add a package from Package Manager window](./Documentation~/Images/add-package.png)
![Type package URL](./Documentation~/Images/set-package-url.png)

### Update the package

Just do the install operations again to update the package in your project.

## Documentation

[=> See full documentation](./Documentation~/README.md)