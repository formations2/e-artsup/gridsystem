using System.Collections.Generic;

using UnityEngine;

namespace HG.GridSystem
{

    ///<summary>
    /// Contains the settings of a 2D grid graph layout.
    ///</summary>
    [CreateAssetMenu(fileName = "NewGrid2DLayout", menuName = "Grid System/Grid 2D Layout")]
    public class Grid2DLayoutAsset : ScriptableObject
    {

        #region Fields

        [SerializeField]
        [Tooltip("Contains the position of all the non-crossable nodes on the grid.")]
        private List<Vector2Int> _walls = new List<Vector2Int>();

        [SerializeField, Min(1)]
        [Tooltip("The size of the grid along the X axis.")]
        private int _width = 4;

        [SerializeField, Min(1)]
        [Tooltip("The size of the grid along the Y axis.")]
        private int _height = 4;

        #endregion


        #region Public API

        /// <inheritdoc cref="_walls"/>
        public Vector2Int[] Walls => _walls.ToArray();

        /// <inheritdoc cref="_width"/>
        public int Width => _width;

        /// <inheritdoc cref="_height"/>
        public int Height => _height;

        /// <summary>
        /// Adds a wall at the given position.
        /// </summary>
        public void AddWallAt(int x, int y)
        {
            foreach (Vector2Int w in _walls)
            {
                if (w.x == x && w.y == y)
                {
                    return;
                }
            }
            _walls.Add(new Vector2Int(x, y));
        }

        /// <summary>
        /// Checks if the node at the given position is a wall (non-crossable).
        /// </summary>
        public bool IsWall(int x, int y)
        {
            foreach (Vector2Int w in _walls)
            {
                if (w.x == x && w.y == y)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes a wall at the given position.
        /// </summary>
        public void RemoveWallAt(int x, int y)
        {
            for (int i = 0; i < _walls.Count; i++)
            {
                if (_walls[i].x == x && _walls[i].y == y)
                {
                    _walls.RemoveAt(i);
                    return;
                }
            }
        }

        #endregion

    }

}