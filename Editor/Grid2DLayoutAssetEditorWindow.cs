using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace HG.GridSystem
{

	///<summary>
	/// Custom editor window for easily drawing a grid layout.
	///</summary>
	public class Grid2DLayoutAssetEditorWindow : EditorWindow
	{

        #region Fields

        private const string EDITOR_WINDOW_TITLE = "Grid 2D Layout";
        private const string WIDTH_PROP = "_width";
        private const string HEIGHT_PROP = "_height";
        private const string WALLS_PROP = "_walls";

        private const float RECT_MARGIN = 2f;

        /// <summary>
        /// The edited layout asset.
        /// </summary>
        [SerializeField]
		private Grid2DLayoutAsset _layoutAsset = null;

        [SerializeField]
        private Color _emptyNodeColor = Color.white;

        [SerializeField]
        private Color _wallNodeColor = Color.black;

        private SerializedObject _layoutAssetObjSerialized = null;

        private SerializedProperty _widthProp = null;
        private SerializedProperty _heightProp = null;
        private SerializedProperty _wallsProp = null;

        private Rect _gridRect = default;
        private Rect[,] _nodeRects = null;

        private bool _isDrawing = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this window is opened.<br/>
        /// This makes the window reload-proof.
        /// </summary>
        private void OnEnable()
        {
            LayoutAsset = _layoutAsset;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window for editing the given layout asset.
        /// </summary>
        /// <param name="layoutAsset">The layout asset you want to edit.</param>
        public static void OpenWindow(Grid2DLayoutAsset layoutAsset)
		{
			// Cancel if the given layout asset is not valid
			if (layoutAsset == null)
				return;

			Grid2DLayoutAssetEditorWindow window = GetWindow<Grid2DLayoutAssetEditorWindow>(false, EDITOR_WINDOW_TITLE, true);
            window.LayoutAsset = layoutAsset;
            window.Show();
		}

        #endregion


        #region UI

		/// <summary>
		/// Draws the GUI of this window.
		/// </summary>
        private void OnGUI()
        {
            if (_layoutAsset == null)
                Close();

            EditorGUILayout.PropertyField(_widthProp);
            EditorGUILayout.PropertyField(_heightProp);

            _emptyNodeColor = EditorGUILayout.ColorField("Empty Node Color", _emptyNodeColor);
            _wallNodeColor = EditorGUILayout.ColorField("Wall Node Color", _wallNodeColor);

            EditorGUILayout.HelpBox("You can draw empty nodes (crossables) using left-click, and wall nodes (non-crossables) using right-click on the grid.", MessageType.Info);

            Rect gridRect = EditorGUILayout.GetControlRect(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            DrawGridGUI(gridRect);

            ProcessMouseEvents();

            _layoutAssetObjSerialized.ApplyModifiedProperties();
        }

        /// <summary>
        /// Draws the grid in the GUI.
        /// </summary>
        /// <param name="gridRect">The available space to draw the grid.</param>
        private void DrawGridGUI(Rect gridRect)
        {
            _gridRect = gridRect;
            _nodeRects = ComputeGridRects(gridRect);
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    EditorGUI.DrawRect(_nodeRects[x, y], _layoutAsset.IsWall(x, y) ? _wallNodeColor : _emptyNodeColor);
                }
            }
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets the width of the edited layout assset.
        /// </summary>
        private int Width => _widthProp != null ? _widthProp.intValue : 0;

        /// <summary>
        /// Gets the height of the edited layout asset. 
        /// </summary>
        private int Height => _heightProp != null ? _heightProp.intValue : 0;

        /// <summary>
        /// Gets/sets the editedd layout asset.
        /// </summary>
        private Grid2DLayoutAsset LayoutAsset
        {
            get { return _layoutAsset; }
            set
            {
                _layoutAsset = value;

                _layoutAssetObjSerialized = new SerializedObject(_layoutAsset);
                _widthProp = _layoutAssetObjSerialized.FindProperty(WIDTH_PROP);
                _heightProp = _layoutAssetObjSerialized.FindProperty(HEIGHT_PROP);
                _wallsProp = _layoutAssetObjSerialized.FindProperty(WALLS_PROP);
            }
        }

        /// <summary>
        /// Called when an asset is double-clicked.
        /// </summary>
        [OnOpenAsset]
		private static bool OnOpenAsset(int instanceID, int line)
        {
			Object obj = EditorUtility.InstanceIDToObject(instanceID);
			if (obj is Grid2DLayoutAsset)
            {
				OpenWindow(obj as Grid2DLayoutAsset);
				return true;
            }
			return false;
        }

        /// <summary>
        /// Computes the rects positions of the grid.
        /// </summary>
        /// <param name="gridSpace">The available space to draw the grid.</param>
        /// <returns>Returns the computed rects.</returns>
        private Rect[,] ComputeGridRects(Rect gridSpace)
        {
            Vector2 rectSize = new Vector2(gridSpace.width / Width - RECT_MARGIN, gridSpace.height / Height - RECT_MARGIN);
            Rect[,] rects = new Rect[Width, Height];

            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    rects[x, y] = new Rect
                    (
                        gridSpace.x + rectSize.x * x + RECT_MARGIN * x,
                        gridSpace.y + rectSize.y * y + RECT_MARGIN * y,
                        rectSize.x,
                        rectSize.y
                    );
                }
            }

            return rects;
        }

        /// <summary>
        /// Processes the current event if it uses the mouse.
        /// </summary>
        private void ProcessMouseEvents()
        {
            if (Event.current.isMouse && _gridRect.Contains(Event.current.mousePosition))
            {
                if (Event.current.button == 0 || Event.current.button == 1)
                {
                    if (Event.current.type == EventType.MouseDown)
                    {
                        _isDrawing = true;
                        DrawNode(Event.current.button == 0, Event.current.mousePosition);
                        Event.current.Use();
                    }
                    else if (Event.current.type == EventType.MouseUp)
                    {
                        _isDrawing = false;
                        Event.current.Use();
                    }
                    else if(_isDrawing && Event.current.type == EventType.MouseDrag)
                    {
                        DrawNode(Event.current.button == 0, Event.current.mousePosition);
                        Event.current.Use();
                    }
                }
            }
        }

        /// <summary>
        /// Adds or removes a wall from the layout.
        /// </summary>
        /// <param name="emptyNode">Is the node to draw crossable?</param>
        /// <param name="position">The position of the cursor on screen.</param>
        private void DrawNode(bool emptyNode, Vector2 position)
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (_nodeRects[x, y].Contains(position))
                    {
                        if (emptyNode)
                            _layoutAsset.RemoveWallAt(x, y);
                        else
                            _layoutAsset.AddWallAt(x, y);
                    }
                }
            }
        }

        #endregion

    }

}